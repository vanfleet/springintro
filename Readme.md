#Spring Intro

##Technology:
* Spring / Spring Boot
* Maven (pom.xml)
* Thymeleaf - Templating engine
* AOP (Aspect-Oriented Programming)
* Spring JPA (Java Persistent API)
* H2 Database

##Setup:
* Java 8 or higher
* IDE (Integrated Development Environment)
    * IntelliJ (With License - required for Spring)
    * Spring Tool Suite (Optional)
* Postman
    
Create new Project using the Spring initializer to include the following:

* Developer Tools
    * Spring Boot DevTools
* Web
	* spring Web Starter
* Template Engines
	* Thymeleaf
* SQL
	* Spring Data JPA
	* H2 Database
* Ops
	* Spring Boot Actuator
	
###Pom File

Look at the different dependencies that have been added to the pom.xml file based on your 
selections in the Spring initializer. Also, add the following dependencies to add support for 
AOP and Model Mapper:

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
        </dependency>
        
        <dependency>
            <groupId>org.modelmapper</groupId>
            <artifactId>modelmapper</artifactId>
            <version>2.0.0</version>
        </dependency>
	
##Overview of Steps:

###Controllers
####Hello World
* Create HelloWorldController class with GET end points
* Create simple Thymeleaf template that receives first and last name on page.
	
###ToDo Application
* Create TodoDto class
* Create TodoController with POST Mapping for /todo which accepts Todo class, and returns the same todo object.
* Create TodoService, with a createTodo method

```
Spring Beans
------------
Several ways to create managed beans:
* Option 1: Create TodoConfig @Configuration file, create @Bean for TodoService, then  
  Add @Autowired to TodoController
* Option 2: Add @Service to TodoService, and remove Todo @Configuration
* XML configuration

Dependency Injection
--------------------
Then general rule is that if the bean is required then use Constructor Injection, if it is 
optional then use setter method injection, and field injection should be avoided.
* Field Injection (@Autowired)
* Setter method injection
* Constructor Injection
```

####Call service
* Inject the TodoService into the TodoController using Constructor Injection
* Call the createTodo method on the service from the Controller.

###Database Setup
* Create Entity Object
* Create TodoRepository interface
* Inject TodoRepository into TodoService
* Convert TodoDto into Todo entity object then save entity object to DB

###AOP Setup
* Create TodoService Aspect for logging
* Create ExecutionTime Annotation
* Create ExecutionTimeAspect
* Add ExecutionTime annotation to TodoService 