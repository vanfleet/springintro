package com.chg.springintro.springintro.services;

import com.chg.springintro.springintro.annotations.ExecutionTime;
import com.chg.springintro.springintro.model.TodoDto;
import com.chg.springintro.springintro.model.db.Todo;
import com.chg.springintro.springintro.repositories.TodoRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {

    private TodoRepository todoRepository;
    private String propValue;

    ModelMapper modelMapper = new ModelMapper();

    public TodoService(TodoRepository todoRepository,
            @Value("${application.prop.value}") String propValue) {
        this.todoRepository=todoRepository;
        this.propValue=propValue;
    }

    @PostConstruct
    public void init() {
        System.out.println("Creating TodoService");
        System.out.println("Prop Value from application.properties: " + propValue);

        createTodo(new TodoDto("Finish this Todo app","Entertainment"));
    }

    @ExecutionTime
    public List<TodoDto> getTodoList() {
        // Get all the todos from the database convert them to list of DTOs
        List<Todo> todos = todoRepository.findAll();
        List<TodoDto> todoDtoList = modelMapper.map(todos,
                new TypeToken<List<TodoDto>>() {}.getType());

        return todoDtoList;
    }

    @ExecutionTime
    public TodoDto createTodo(TodoDto todoDto)  {
        // Map the Dto to the entity object and save
        Todo todo = modelMapper.map(todoDto, Todo.class);
        todo = todoRepository.save(todo);

        // Map the saved entity object back to the Dto and return
        todoDto = modelMapper.map(todo, TodoDto.class);
        return todoDto;
    }

    @ExecutionTime
    public void deleteTodo(TodoDto todoDto)  {
        todoRepository.deleteById(todoDto.getId());
    }
}
