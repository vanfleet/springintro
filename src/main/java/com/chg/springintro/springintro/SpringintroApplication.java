package com.chg.springintro.springintro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class SpringintroApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringintroApplication.class, args);
    }

}
