package com.chg.springintro.springintro.repositories;

import com.chg.springintro.springintro.model.db.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
