package com.chg.springintro.springintro.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HelloWorldController {

    @GetMapping("/hi")
    @ResponseBody
    public String hi() {
        return "Hello";
    }

    @GetMapping("/hello")
    public String getHello(@RequestParam String firstName, @RequestParam String lastName, Model model) {
        model.addAttribute("firstName",firstName);
        model.addAttribute("lastName",lastName);
        return "hello"; // Thymeleaf Template
    }

}
