package com.chg.springintro.springintro.controllers;

import com.chg.springintro.springintro.model.TodoDto;
import com.chg.springintro.springintro.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/todolist")
    public String getTodoList(Model model) {
        List<TodoDto> todoList = todoService.getTodoList();
        model.addAttribute("todoList",todoList);
        return "todo"; // Open thymeleaf Template
    }

    @PostMapping("/todolist")
    @ResponseBody
    public List<TodoDto> postTodoList() {
        return todoService.getTodoList();
    }

    @PostMapping("/todo")
    @ResponseBody
    public TodoDto createTodo(@RequestBody TodoDto todoItem) {
        TodoDto todo = todoService.createTodo(todoItem);
        return todo;
    }

    @DeleteMapping("/todo")
    @ResponseBody
    public void deleteTodo(@RequestBody TodoDto todoItem) {
        todoService.deleteTodo(todoItem);
    }

}
