package com.chg.springintro.springintro.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TodoServiceAspect {

    @Before(value = "execution(* com.chg.springintro.springintro.services.TodoService.*(..))")
    public void beforeAdvice(JoinPoint joinPoint) {
        System.out.println("Before method:" + joinPoint.getSignature());
        System.out.println("Args: " + joinPoint.getArgs().length);
        for (Object arg:joinPoint.getArgs()) {
            System.out.println("  Arg: " + arg);
        }
    }

//    @Before(value = "execution(* com.chg.springintro.springintro.services.TodoService.createTodo(java.lang.String,java.lang.String)) && args(todo, cat)")
//    public void beforeAdvice(JoinPoint joinPoint, String todo, String cat) {
//        System.out.println("Before method:" + joinPoint.getSignature());
//        System.out.println("created todo: " + todo + " - " + cat);
//    }

//    @After(value = "execution(* com.chg.springintro.springintro.services.TodoService.*(..)) && args(todo,cat)")
//    public void afterAdvice(JoinPoint joinPoint, String todo, String cat) {
//        System.out.println("After method:" + joinPoint.getSignature());
//
//        System.out.println("Successfully created todo - " + todo + " - " + cat);
//    }
}
